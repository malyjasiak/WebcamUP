﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows.Forms;
using System.Windows.Threading;
using Accord.IO;
using Accord.Video.FFMPEG;
using AForge.Video;
using AForge.Video.DirectShow;
using DirectShowLib;
using CameraControlFlags = DirectShowLib.CameraControlFlags;
using CameraControlProperty = DirectShowLib.CameraControlProperty;
using FilterCategory = AForge.Video.DirectShow.FilterCategory;
using FilterInfo = AForge.Video.DirectShow.FilterInfo;
using MessageBox = System.Windows.MessageBox;
using System.Threading;
using AForge.Imaging.Filters;


namespace WebCam
{
    public partial class MainWindow : Window
    {
        private bool grey = false;
        private float saturationAmount = 0.0f;



        private bool DeviceExist = false;
        private FilterInfoCollection videoDevices;
        private VideoCaptureDevice videoSource = null;
        private VideoCaptureDeviceForm captureDeviceForm;
        private readonly DispatcherTimer timer = new DispatcherTimer();
        private VideoFileWriter FileWriter = new VideoFileWriter();
        private SaveFileDialog saveAvi;
        private Bitmap videoFrame;
        private IAMCameraControl cameraControl;
        private bool recording = false;

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr value);

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            

            timer.Tick += timer1_Tick;
            timer.Interval = new TimeSpan(0, 0, 1);
            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            captureDeviceForm = new VideoCaptureDeviceForm();
            try
            {
                videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                DeviceExist = true;
                foreach (FilterInfo device in videoDevices)
                {
                    comboBox.Items.Add(device.Name);
                }
                comboBox.SelectedIndex = 0; //make dafault to first cam
            }
            catch (ApplicationException)
            {
                DeviceExist = false;
                comboBox.Items.Add("No capture device on your system");
            }
        }

 

        private void Button_OnClick(object sender, RoutedEventArgs e)
        {
            if (videoSource == null || !videoSource.IsRunning)
            {
                if (DeviceExist)
                {
                    var mon = videoDevices[comboBox.SelectedIndex].MonikerString;
                    var name = videoDevices[comboBox.SelectedIndex].Name;

                    videoSource = new VideoCaptureDevice(videoDevices[comboBox.SelectedIndex].MonikerString);
                    videoSource.NewFrame += video_NewFrame;
                    videoSource.Start();
                    //System.Threading.Thread.Sleep(2000);
                    //cameraControl = (IAMCameraControl)videoSource.SourceObject;
                    //cameraControl.Set(CameraControlProperty.Exposure, -11, CameraControlFlags.Manual);
                    DsDevice[] devs = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
                    IFilterGraph2 graphBuilder = new FilterGraph() as IFilterGraph2;
                    IBaseFilter capFilter = null;
                    int hr = graphBuilder.AddSourceFilterForMoniker(devs[comboBox.SelectedIndex].Mon, null, devs[comboBox.SelectedIndex].Name, out capFilter);
                    DsError.ThrowExceptionForHR(hr);
                    cameraControl = capFilter as IAMCameraControl;
                    
                    cameraControl.Set(CameraControlProperty.Exposure, -5, CameraControlFlags.Manual);
                    cameraControl.Set(CameraControlProperty.Zoom, 100, CameraControlFlags.Manual);



                    TextBox.Text = "Device running...";

                    timer.IsEnabled = true;
                }
                else
                {
                    TextBox.Text = "Error: No Device selected.";
                }
            }
            else
            {
                timer.IsEnabled = false;
                
                CloseVideoSource();
            }
        }

        private void CloseVideoSource()
        {
            if (videoSource == null) return;
            if (!videoSource.IsRunning) return;
            FileWriter.Close();
            videoSource.SignalToStop();
            videoSource = null;
        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            videoFrame = (Bitmap)eventArgs.Frame.Clone();
            this.Dispatcher.Invoke(() =>
                {
                    CameraView.Source = GetImageStream(videoFrame);
                }
            );
            if(recording && FileWriter.IsOpen) FileWriter.WriteVideoFrame(videoFrame);
        }

        private Bitmap ApplyFilters(Bitmap b)
        {

            if (grey)
            {
                Grayscale grayscale = new Grayscale(0.2125, 0.7154, 0.0721);
                b = grayscale.Apply(b);
            }

            else
            {
                SaturationCorrection sat = new SaturationCorrection(saturationAmount);

                b = sat.Apply(b);
                
            }
            return b;
        }


    public BitmapSource GetImageStream(Image myImage)
        {
            var bitmap = ApplyFilters(new Bitmap(myImage));
            IntPtr bmpPt = bitmap.GetHbitmap();
            BitmapSource bitmapSource =
             System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                   bmpPt,
                   IntPtr.Zero,
                   Int32Rect.Empty,
                   BitmapSizeOptions.FromEmptyOptions());

            bitmapSource.Freeze();
            DeleteObject(bmpPt);

            return bitmapSource;
        }

         private void timer1_Tick(object sender, EventArgs e)
         {
             TextBox.Text = "Device running... " + videoSource.FramesReceived + " FPS";

         }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            CloseVideoSource();
        }

        private void ButtonVideo_OnClick(object sender, RoutedEventArgs e)
        {
            if (!recording)
            { 
                saveAvi = new SaveFileDialog();
                saveAvi.Filter = "Avi Files (*.avi)|*.avi";
                if (saveAvi.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    int h = videoFrame.Height;
                    int w = videoFrame.Width;
                    FileWriter.Open(saveAvi.FileName, w, h, 15, VideoCodec.Default, 5000000);
                    recording = true;
                    ButtonVideo.Content = "Zatrzymaj Nagrywanie";
                }
               
            }
            else
            {
                ButtonVideo.Content = "Nagrywak";
                recording = false;
                if (videoSource.IsRunning)
                {
                    FileWriter.Close();
                }
            }
        }

        private void ButtonPicture_OnClick(object sender, RoutedEventArgs e)
        { 

            var bitmap = new Bitmap(videoFrame);
            bitmap.Save($"C:/Users/Kuba/Desktop/{DateTime.Now.ToFileTime()}.png", ImageFormat.Png);
            bitmap.Dispose();
        }

        private void SliderExposure_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            cameraControl?.Set(CameraControlProperty.Exposure, Convert.ToInt32(SliderExposure.Value),
                CameraControlFlags.Manual);
        }

        private void SliderZoom_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            saturationAmount = (float) SliderZoom.Value / 100;
        }

        private void CheckBoxGrey_OnClick(object sender, RoutedEventArgs e)
        {
            if (CheckBoxGrey.IsChecked != null) grey = CheckBoxGrey.IsChecked.Value;
        }
    }
}
